from math import ceil
from datetime import datetime
from random import random, uniform
from calendar import monthrange
import psycopg2


class DataManager:
    """Mock data generation, accumulation rates computation and scheduling for fill levels."""

    def __init__(self, connection_string, container_table_name, fill_level_table_name, container_id_column):
        """Pass connection string of form 'port=5433 dbname=mydbname user=postgres password=abcd'."""
        self.connection_string = connection_string
        self.container_table = container_table_name
        self.fill_level_table = fill_level_table_name
        self.container_id_column = container_id_column
        self.connect()

    def connect(self):
        """Open db connection using Psycopg2."""
        self.connection = psycopg2.connect(self.connection_string)
        self.cursor = self.connection.cursor()
        # save the container ids for the given table
        self.cursor.execute(f"select distinct {self.container_id_column} from {self.container_table}")
        self.container_ids = [x[0] for x in self.cursor.fetchall()]

    def disconnect(self):
        """Close db connection."""
        if self.connection:
            self.cursor.close()
            self.connection.close()

    def create_fill_level_table(self):
        """Is called internally by generate_fill_levels."""
        self.cursor.execute(f"""
            drop table if exists {self.fill_level_table};
            create table {self.fill_level_table}(
                record_id bigserial primary key,
                recorded_on date not null default current_date,
                container_id int references {self.container_table}(fid),
                fill_level float not null check(fill_level >= 0 and fill_level <= 1),
                increase float not null check(increase >= 0))
        """)
        self.connection.commit()

    def calculate_waste_amounts(self, container_table_name, fill_level_table_name):
        """Uses capacity_kg (container table) and fill level (fill levels table) to define the absolute amount of waste in container."""
        self.cursor.execute(f"""
            update {container_table_name}
            set amount_kg = capacity_kg * fill_level
            from {fill_level_table_name}
            where {fill_level_table_name}.container_id = {container_table_name}.fid
        """)
        self.connection.commit()

    def generate_fill_levels(self, months):
        """Initial levels (day 1) are generated randomly; every next day a random value (0.05, 0.2) is added."""
        self.create_fill_level_table()
        now = datetime.now()
        months = [((month, monthrange(2019, month)[1]) if month != now.month else (month, now.day - 1)) for month in months]

        for month in months:
            print(f'Generating fill levels for {month}...')
            for container_id in self.container_ids:
                level = random()
                increase = 0
                for day in range(1, month[1] + 1):
                    self.cursor.execute(f"""
                        insert into {self.fill_level_table} (recorded_on, container_id, fill_level, increase)
                        values('2019-{month[0]}-{day}', {container_id}, {round(level, 2)}, {increase});
                    """)
                    increase = round(uniform(0.05, 0.2), 2)
                    new_level = level + increase
                    level = new_level if new_level < 1 else (new_level - 1)

                    self.connection.commit()

        print('Calculating waste amounts...')
        self.calculate_waste_amounts(self.container_table, self.fill_level_table)

        print('Done!')

    def compute_mean_acc_rates(self):
        """Get simple mean average of all records for given container."""
        for container_id in self.container_ids:
            self.cursor.execute(f"select avg(increase) from fill_levels where container_id = {container_id}")
            rate = round(self.cursor.fetchone()[0], 2)
            self.cursor.execute(f"update {self.container_table} set mean_acc_rate = {rate} where fid = {container_id}")
            self.connection.commit()

    def correct_schedule(self, pickup_date, current_date):
        """Make sure there are no containers scheduled for weekends and all dates are valid calendarwise."""
        days_in_current_month = monthrange(current_date.year, current_date.month)[1]
        # for cases like 'April 40th'
        if pickup_date['day'] > days_in_current_month:
            pickup_date['month'] += 1
            pickup_date['day'] -= days_in_current_month
        # exclude Sat & Sun
        weekday = datetime(2019, pickup_date['month'], pickup_date['day']).weekday()
        if weekday == 5:
            pickup_date['day'] -= 1
        if weekday == 6:
            pickup_date['day'] -= 2
        # if previous step resulted in days < 1
        if pickup_date['day'] < 1:
            pickup_date['day'] -= monthrange(current_date.year, pickup_date['month']) + pickup_date['day']
            pickup_date['month'] -= 1

    def schedule(self):
        """Assuming levels are recorded at the end of the day => overflow day is the default collection day."""
        now = datetime.now()
        for container_id in self.container_ids:
            # fetch and store accumulation rate for the container
            self.cursor.execute(f"select mean_acc_rate from {self.container_table} where fid = {container_id}")
            rate = self.cursor.fetchone()[0]
            # fetch and store current fill level for the container
            self.cursor.execute(f"""
                select fill_level, recorded_on 
                from {self.fill_level_table} 
                where container_id = {container_id} 
                order by recorded_on desc
            """)
            current_level = self.cursor.fetchone()[0]
            # fill levels are measured at the end of the day (change if moving to start-of-the-day measurements)
            days_till_overflow = ceil((1 - current_level) / rate)
            pickup_date = {
                "year": now.year,
                "month": now.month,
                "day": now.day + days_till_overflow
            }
            # Take care of Sat, Sun and dates like April 40th or May -1th
            self.correct_schedule(pickup_date, now)
            # write results to db
            self.cursor.execute(f"""
                update {self.container_table}
                set collect_on = '{pickup_date["year"]}-{pickup_date["month"]}-{pickup_date["day"]}'
                where fid = {container_id}
            """)
            self.connection.commit()

    def get_dates(self):
        """Get a list of scheduled collection dates."""
        self.connect()
        self.cursor.execute(f"select distinct collect_on from {self.container_table}")
        dates = [row[0] for row in self.cursor.fetchall()]
        self.disconnect()
        return [f"{date.year}-{date.month}-{date.day}" for date in dates]


if __name__ == '__main__':
    try:
        dm = DataManager("port=5433 dbname=almere user=postgres password=301095Gr", 'containers_20', 'fill_levels', 'fid')
        dm.connect()
        dm.generate_fill_levels([1, 2, 3, 4, 5, 6])
    finally:
        dm.disconnect()
