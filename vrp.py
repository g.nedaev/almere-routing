from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver.pywrapcp import RoutingIndexManager, RoutingModel, DefaultRoutingSearchParameters
from pathlib import Path
import numpy as np
import json


def get_num_pts(least_cost_paths):
    """Automatically defines the number of visit locations (incl. depot)."""
    with open(least_cost_paths) as f:
        data = json.load(f)
        return len(set([feature['properties']['to_node'] for feature in data['features']]))


def create_cost_matrix(num_pts, json_input):
    """Convert a least-cost paths GeoJSON file into a cost matrix.
Number of points (locations) including depot. Depot must have the smallest fid 
JSON input's 'properties' attribute must contain 'from_node', 'to_node' and 'cost'.
Costs will be rounded to the nearest integer.
    """
    with open(json_input) as f:
        data = json.load(f)
        matrix = np.zeros((num_pts, num_pts), int)
        features = [feature['properties'] for feature in data['features']]
        feature_ids = sorted(list(set([feature['to_node'] for feature in features])))
        for feature in features:
            row_number = feature_ids.index(feature['from_node'])
            col_number = feature_ids.index(feature['to_node'])
            matrix[row_number][col_number] = round(feature['cost'])

        return {'matrix': matrix, 'fids': feature_ids}


def print_solution(num_vehicles, manager, routing, solution, fids, waste_amounts):
    """Prints solution on console."""
    max_route_cost = 0
    for vehicle_id in range(num_vehicles):
        index = routing.Start(vehicle_id)
        plan_output = f'Route for vehicle {vehicle_id}:\n'
        route_cost = 0
        while not routing.IsEnd(index):
            # converts model index to node index
            plan_output += f' {fids[manager.IndexToNode(index)]} -> '
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_cost += routing.GetArcCostForVehicle(previous_index, index, vehicle_id)

        plan_output += f'{fids[manager.IndexToNode(index)]}\n'
        plan_output += f'Duration of the route: {route_cost}m\n'
        print(plan_output)
        max_route_cost = max(route_cost, max_route_cost)
    print(f'Longest route: {max_route_cost}m')


def get_stats(num_vehicles, manager, routing, solution, fids, waste_amounts):
    """"""
    routes = []
    costs = []
    waste_collected_all_vehicles = []
    for vehicle_index in range(num_vehicles):
        route = []
        cost = 0
        waste_collected = 0
        index = routing.Start(vehicle_index)
        while not routing.IsEnd(index):
            fid = fids[manager.IndexToNode(index)]
            route.append(fid)
            next_index = solution.Value(routing.NextVar(index))
            cost += routing.GetArcCostForVehicle(index, next_index, vehicle_index)
            waste_collected += waste_amounts[fid]
            index = next_index
        else:
            routes.append(route)
            costs.append(cost)
            waste_collected_all_vehicles.append(round(waste_collected, 1))
    return {'routes': routes, 'costs': costs, 'waste_collected': waste_collected_all_vehicles, 'num_vehicles': num_vehicles}


def write_solution(folder, date, stats):
    """"""
    routes, costs, waste_collected, num_vehicles = stats.values()
    with open(folder / f'least_cost_paths_{date}.geojson') as least_cost_paths:
        data = json.load(least_cost_paths)
        old_features = data['features']
        data['name'] = 'solution'
        new_features = []
        for vehicle_index in range(num_vehicles):
            route = routes[vehicle_index]
            feature = {
                "type": "Feature",
                "properties": {
                    "fid": vehicle_index + 1,  # to make it 1-based
                    "duration_min": costs[vehicle_index],  # duration in minutes
                    "waste_collected_kg": waste_collected[vehicle_index],
                    "#containers": len(route)  # number of containers
                },
                "geometry": {"type": "MultiLineString", "coordinates": []}
            }

            geom_complete = []

            for i in range(len(route) - 1):
                for x in old_features:
                    if x['properties']['from_node'] == route[i] and x['properties']['to_node'] == route[i + 1]:
                        geom = x['geometry']['coordinates']
                        geom_complete.extend(geom)
                        break

            feature['geometry']['coordinates'] = geom_complete
            new_features.append(feature)

        data['features'] = new_features
        with open(folder / f"solution_{date}.geojson", 'w') as f:
            json.dump(data, f)


def solve(folder, date, vehicle_num_range, vehicle_capacity, max_route_duration, waste_amounts, depot_index=0, minutes_per_container=1):
    """Solve the CVRP problem."""
    # reconstruct least_cost_paths file address
    least_cost_paths = folder / f"least_cost_paths_{date}.geojson"
    # define number of locations (incl. depot)
    num_pts = get_num_pts(least_cost_paths)
    # convert GeoJSON into a matrix and also get the orignal fid for each container
    cost_matrix, fids = create_cost_matrix(num_pts, least_cost_paths).values()
    # objective - minimize fleet size
    for num_vehicles in vehicle_num_range:
        # Index manager keeps the relation between the indices of our containers in distance matrix and variables created internally by the solver
        # If your vehicles start at the depot but end at another point (recycling plant or sth), you can add that location's index in distance matrix as the 4th argument
        manager = RoutingIndexManager(num_pts, num_vehicles, depot_index)
        routing = RoutingModel(manager)

        def transit_callback(from_index, to_index):
            """Returns travel time between two nodes. Converts internal indices used by the solver to indices for the matrix."""
            from_node = manager.IndexToNode(from_index)
            to_node = manager.IndexToNode(to_index)
            return cost_matrix[from_node][to_node] + minutes_per_container

        transit_callback_index = routing.RegisterTransitCallback(transit_callback)
        routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

        # Add duration constraint
        dimension_name = 'Time'
        routing.AddDimension(
            transit_callback_index,
            0,  # no slack
            max_route_duration,  # in min
            True,  # start cumul to zero
            dimension_name)

        time_dimension = routing.GetDimensionOrDie(dimension_name)
        time_dimension.SetGlobalSpanCostCoefficient(100)

        # add capacity constraint
        def capacity_callback(from_index):
            """Returns waste amount at next container."""
            from_node = manager.IndexToNode(from_index)
            return waste_amounts[fids[from_node]]

        capacity_callback_index = routing.RegisterUnaryTransitCallback(capacity_callback)
        routing.AddDimensionWithVehicleCapacity(
            capacity_callback_index,
            0,  # no slack
            num_vehicles * [vehicle_capacity],  # same capacity for all vehicles
            True,  # start at zero
            'Capacity')

        # Set solution heuristic
        search_parameters = DefaultRoutingSearchParameters()
        # choose the method to produce the start solution for local search; see https://developers.google.com/optimization/routing/routing_options
        search_parameters.first_solution_strategy = (routing_enums_pb2.FirstSolutionStrategy.PARALLEL_CHEAPEST_INSERTION)
        # choose metaheuristic; see https://developers.google.com/optimization/routing/routing_options
        search_parameters.local_search_metaheuristic = (routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
        search_parameters.time_limit.seconds = 20  # time limit for the metaheuristic
        # Solve the problem.
        solution = routing.SolveWithParameters(search_parameters)
        if solution:
            write_solution(folder, date, get_stats(num_vehicles, manager, routing, solution, fids, waste_amounts))
            break
        # figure out what to do otherwise
        else:
            QMessageBox(QMessageBox.Information, self.plugin_name, 'Not enough vehicles!', parent=self.main_window).open()
