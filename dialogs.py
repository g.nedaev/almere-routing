# -*- coding: utf-8 -*-
import os
from PyQt5 import uic
from PyQt5 import QtWidgets

# This loads your .ui file so that PyQt can populate your plugin with the elements from Qt Designer
FORM_CLASS_SETUP = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'almere_garbage_collection_dialog_base.ui'))[0]


class SetupDialog(QtWidgets.QDialog, FORM_CLASS_SETUP):
    def __init__(self, parent=None):
        super(SetupDialog, self).__init__(parent)
        self.setupUi(self)
