# -*- coding: utf-8 -*-
from qgis.core import QgsProject, QgsVectorLayer, QgsDataSourceUri, QgsExpression, QgsCoordinateReferenceSystem, QgsFeature, QgsGeometry, QgsPointXY, QgsCoordinateTransform, Qgis, QgsField
from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMessageBox
from .resources import *
from .dialogs import SetupDialog
from .data_manager import DataManager
from .vrp import solve
from pathlib import Path
from psycopg2 import OperationalError, ProgrammingError
import os
import json
import processing
import time


class AlmereGarbageCollection:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor."""
        self.iface = iface
        self.canvas = iface.mapCanvas()
        self.project = QgsProject.instance()
        self.main_window = iface.mainWindow()
        self.msg_bar = iface.messageBar()
        self.plugin_name = 'Almere Garbage Collection'
        self.plugin_dir = Path(os.path.dirname(__file__))
        self.dlg_setup = SetupDialog()
        self.temp_folder = Path(self.plugin_dir / 'temp_data')
        self.actions = []

    def add_action(self, icon_path, text, callback, enabled_flag=False):
        """Add clickable buttons that execute assigned functions."""
        icon = QIcon(icon_path)
        action = QAction(icon, text, self.main_window)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)
        self.iface.addToolBarIcon(action)
        self.iface.addPluginToMenu(self.plugin_name, action)
        self.actions.append(action)
        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""
        setup_path = ':/plugins/almere_garbage_collection/icons/setup.png'
        road_network_to_graph_path = ':/plugins/almere_garbage_collection/icons/road_network_to_graph.png'
        schedule_path = ':/plugins/almere_garbage_collection/icons/schedule.png'
        display_container_layers_path = ':/plugins/almere_garbage_collection/icons/display_container_layers.png'
        vrp_path = ':/plugins/almere_garbage_collection/icons/vrp.png'
        self.add_action(setup_path, 'Setup the plugin', self.setup, enabled_flag=True)
        self.add_action(road_network_to_graph_path, 'Convert road network into graph', self.road_network_to_graph)
        self.add_action(schedule_path, 'Schedule Collection Dates', self.schedule)
        self.add_action(display_container_layers_path, 'Show Containers', self.display_container_layers)
        self.add_action(vrp_path, 'Find Routes', self.solve_vrp)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.plugin_name, action)
            self.iface.removeToolBarIcon(action)

    def setup(self):
        """Specify data sources and VRP parameters."""
        self.dlg_setup.show()
        ok_clicked = self.dlg_setup.exec_()
        if ok_clicked:
            try:
                # get user input
                self.pg_host = self.dlg_setup.host.text()
                self.pg_port = self.dlg_setup.port.text()
                self.pg_username = self.dlg_setup.username.text()
                self.pg_password = self.dlg_setup.password.text()
                self.pg_database = self.dlg_setup.database.text()
                self.pg_schema = self.dlg_setup.schema.text()
                self.pg_containers_table = self.dlg_setup.containers_table.text()
                self.pg_containers_geom_col = self.dlg_setup.containers_geom_col.text()
                self.pg_containers_id_col = self.dlg_setup.containers_id_col.text()
                self.pg_fill_levels_table = self.dlg_setup.fill_levels_table.text()
                self.pg_roads_table = self.dlg_setup.roads_table.text()
                self.pg_roads_geom_col = self.dlg_setup.roads_geom_col.text()
                self.pg_roads_id_col = self.dlg_setup.roads_id_col.text()
                self.pg_roads_fwd_cost_col = self.dlg_setup.roads_fwd_cost_col.text()
                self.pg_roads_bwd_cost_col = self.dlg_setup.roads_bwd_cost_col.text()
                # get the rest of the input
                self.depot = self.dlg_setup.depot.text().split(',')
                self.max_num_vehicles = int(self.dlg_setup.max_num_vehicles.text())
                self.min_num_vehicles = int(self.dlg_setup.min_num_vehicles.text())
                self.vehicle_capacity = int(self.dlg_setup.vehicle_capacity.text())
                self.max_route_duration = int(self.dlg_setup.max_route_duration.text())
                # use this input
                self.dm = DataManager(
                    f"host={self.pg_host} port={self.pg_port} user={self.pg_username} password={self.pg_password} dbname={self.pg_database}",
                    self.pg_containers_table,
                    self.pg_fill_levels_table,
                    self.pg_containers_id_col
                )
                self.uri_pg = QgsDataSourceUri()
                self.uri_pg.setConnection(self.pg_host, self.pg_port, self.pg_database, self.pg_username, self.pg_password)
                # setup is done! enable the other buttons
                for action in self.actions:
                    action.setEnabled(True)
            except ValueError:
                QMessageBox(QMessageBox.Critical, self.plugin_name, 'One or more input values invalid!', parent=self.main_window).open()
            except (OperationalError, ProgrammingError):
                QMessageBox(QMessageBox.Critical, self.plugin_name, 'Could not connect to the database!', parent=self.main_window).open()

    def load_container_layer(self, collection_date=None):
        """Load containers from Postgres."""
        where_clause = f"collect_on = '{collection_date}'" if collection_date else ""
        layer_name = f"containers_{collection_date}" if collection_date else "containers_all"
        self.uri_pg.setDataSource(self.pg_schema, self.pg_containers_table, self.pg_containers_geom_col, where_clause, self.pg_containers_id_col)
        layer = QgsVectorLayer(self.uri_pg.uri(), layer_name, 'postgres')
        return layer if layer.isValid() else self.msg_bar.pushMessage(self.plugin_name, f'Error loading {layer_name}', level=Qgis.Critical)

    def load_road_layer(self):
        """Load roads from Postgres."""
        self.uri_pg.setDataSource(self.pg_schema, self.pg_roads_table, self.pg_roads_geom_col, '', self.pg_roads_id_col)
        layer_roads = QgsVectorLayer(self.uri_pg.uri(), 'roads', 'postgres')
        return layer_roads if layer_roads.isValid() else self.msg_bar.pushMessage(self.plugin_name, 'Error loading road layer', level=Qgis.Critical)

    def all_pairs(self, containers, roads):
        """Run GRASS All Pairs to get a cost matrix."""
        print('Starting all pairs...')
        start = time.time()
        output_path = self.temp_folder / "all_pairs_raw.geojson"
        try:
            processing.run("grass7:v.net.allpairs", {
                'input': roads,
                'points': containers,
                'threshold': 200,
                'arc_column': self.pg_roads_fwd_cost_col,
                'arc_backward_column': self.pg_roads_bwd_cost_col,
                'output': str(output_path),
                'GRASS_SNAP_TOLERANCE_PARAMETER': -1,
                'GRASS_MIN_AREA_PARAMETER': 0.0001,
                'GRASS_OUTPUT_TYPE_PARAMETER': 0
            })
            main_end = time.time()
            # Resulting GeoJSON has no crs attribute => code below manually adds it. Still ugly.
            print(f'Finished all pairs in {round(main_end - start)} sec')

            with open(output_path, 'r+') as f:
                data = json.load(f)
                data['crs'] = {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::28992"}}
                f.seek(0)
                json.dump(data, f)
                f.truncate()

            end = time.time()
            print(f'Added crs attribute to result in {round(end - main_end)} sec')
        except FileNotFoundError:
            QMessageBox(QMessageBox.Critical, self.plugin_name, 'Coul not generate shortest paths!', parent=self.main_window).open()

    def cats_to_original_fids(self, original_fids):
        """GRASS All Pairs drops the original fids. This function restores them. See 'aggregate'."""
        start = time.time()

        with open(str(self.temp_folder / 'all_pairs.geojson'), 'r+') as f:
            data = json.load(f)
            for feature in data['features']:
                feature['properties']['from_node'] = original_fids[(feature['properties']['from_node'] - 1)]
                feature['properties']['to_node'] = original_fids[(feature['properties']['to_node'] - 1)]
            f.seek(0)
            json.dump(data, f)
            f.truncate()

        end = time.time()
        print(f'Restored original fids in {round(end - start)} sec')

    def aggregate(self, original_fids):
        """GRASS All Pairs returns shortest path by segment. This function aggregates those into complete paths."""
        print('Aggregating...')
        start = time.time()

        processing.run("qgis:aggregate", {
            'INPUT': str(self.temp_folder / 'all_pairs_raw.geojson'),
            'GROUP_BY': '\"cat\"',
            'AGGREGATES': [
                {'aggregate': 'first_value', 'delimiter': ',', 'input': '"cat"',
                    'length': 0, 'name': 'fid', 'precision': 0, 'type': 2},
                {'aggregate': 'first_value', 'delimiter': ',', 'input': '"from_cat"',
                    'length': 0, 'name': 'from_node', 'precision': 0, 'type': 2},
                {'aggregate': 'first_value', 'delimiter': ',', 'input': '"to_cat"',
                    'length': 0, 'name': 'to_node', 'precision': 0, 'type': 2},
                {'aggregate': 'first_value', 'delimiter': ',', 'input': '"cost"',
                    'length': 0, 'name': 'cost', 'precision': 0, 'type': 6}
            ],
            'OUTPUT': str(self.temp_folder / 'all_pairs.geojson')
        })

        end = time.time()
        print(f'Aggregated in {round(end - start)} sec')
        self.cats_to_original_fids(original_fids)

    def add_depot(self, container_layer, depot_coordinates, crs):
        """Add depot to container dataset for all pairs computation. Provide in (lon, lat) RD New."""
        # create memory layer with the same attributes as containers_layer
        new_layer = QgsVectorLayer('Point?crs=epsg:28992&index=yes', 'result', 'memory')
        new_layer.startEditing()
        new_layer.dataProvider().addAttributes(container_layer.fields().toList())
        new_layer.commitChanges()
        # create the depot point
        depot = QgsFeature(new_layer.fields())
        depot.setAttribute(self.pg_containers_id_col, 0)
        wgs_to_rd = QgsCoordinateTransform(QgsCoordinateReferenceSystem(4326), crs, self.project)
        depot_pt = wgs_to_rd.transform(QgsPointXY(float(depot_coordinates[0]), float(depot_coordinates[1])))
        depot.setGeometry(QgsGeometry.fromPointXY(depot_pt))
        # merge the depot with the containers and add them all to the newly created layer
        features = list(container_layer.getFeatures())
        features.append(depot)
        result = new_layer.dataProvider().addFeatures(features)
        return new_layer if result[0] else self.msg_bar.pushMessage(self.plugin_name, 'Could not add depot to container layer!', level=Qgis.Critical)

    def road_network_to_graph(self):
        """Find shortest paths for all pairs of containers on the given road network. Depot default De Steiger 222, give in (lon, lat) RD New."""
        roads = self.load_road_layer()
        containers = self.load_container_layer()
        containers_and_depot = self.add_depot(containers, self.depot, containers.crs())

        print('Starting to convert road network to graph...')
        start = time.time()

        self.all_pairs(containers_and_depot, roads)
        original_fids = [feature[self.pg_containers_id_col] for feature in containers_and_depot.getFeatures()]
        self.aggregate(original_fids)

        end = time.time()
        print(f'Done converting road network to graph in {round(end - start)} sec')
        QMessageBox(QMessageBox.Information, self.plugin_name, 'Least-cost paths generated!', parent=self.main_window).open()

    def schedule(self):
        """Assign collection dates to containers."""
        try:
            self.dm.connect()
            self.dm.compute_mean_acc_rates()
            print('Starting scheduling...')
            self.dm.schedule()
            self.dm.disconnect()
            QMessageBox(QMessageBox.Information, self.plugin_name, 'Scheduling done!', parent=self.main_window).open()
        except TypeError:
            QMessageBox(QMessageBox.Critical, self.plugin_name, 'Specified table has no records!', parent=self.main_window).open()

    def display_container_layers(self):
        """Add containers to map."""
        dates = self.dm.get_dates()
        container_layers = [self.load_container_layer(date) for date in dates]
        current_layers = [layer.name() for layer in self.project.mapLayers().values()]
        for layer in container_layers:
            if layer.name() in current_layers:
                self.project.removeMapLayer(self.project.mapLayersByName(layer.name())[0])

            self.project.addMapLayer(layer)

    def solve_vrp(self):
        """Use OR Tools to solve the VRP for each date and indetify the optimal number of vehicles."""
        try:
            self.dm.connect()
            dates = self.dm.get_dates()
            print(f'Loading all pairs...')
            all_pairs_file = open(self.temp_folder / 'all_pairs.geojson', 'r')
            all_pairs = json.load(all_pairs_file)
            vehicle_num_range = range(self.min_num_vehicles, self.max_num_vehicles + 1)
            for date in dates:
                print(f'Solving VRP for {date}...')
                print('Loading containers...')
                container_layer = self.load_container_layer(date)
                fids = [feature[self.pg_containers_id_col] for feature in container_layer.getFeatures()]
                # append the depot
                fids.append(0)
                # get waste amounts
                amounts = {feature[self.pg_containers_id_col]: feature['amount_kg'] for feature in container_layer.getFeatures()}
                amounts[0] = 0

                print('Extracting least-cost paths...')
                start_extract = time.time()
                data = {
                    "type": "FeatureCollection",
                    "name": f"least_cost_paths_{date}",
                    "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::28992"}},
                    "features": [f for f in all_pairs['features'] if f['properties']['from_node'] in fids and f['properties']['to_node'] in fids]
                }
                end_extract = time.time()
                print(f'Extracting done in {round(end_extract - start_extract)}')

                print('Writing the extract to file...')
                start_write = time.time()
                with open(self.temp_folder / f'least_cost_paths_{date}.geojson', 'w') as f:
                    json.dump(data, f)
                end_write = time.time()
                print(f'Writing done in {round(end_write - start_write)}')

                print('Solving the VRP...')
                start_vrp = time.time()
                solve(self.temp_folder, date, vehicle_num_range, self.vehicle_capacity, self.max_route_duration, amounts)
                end_vrp = time.time()
                print(f'VRP for {date} done in {round(end_vrp - start_vrp)}')

                vrp_solution = QgsVectorLayer(str(self.temp_folder / f'solution_{date}.geojson'), f'solution_{date}', 'ogr')
                vrp_solution.addExpressionField('round($length)', QgsField(name='length_m', type=QVariant.Int))
                self.project.addMapLayer(vrp_solution)

            QMessageBox(QMessageBox.Information, self.plugin_name, 'VRP solved!', parent=self.main_window).open()
        finally:
            self.dm.disconnect()
            all_pairs_file.close()
